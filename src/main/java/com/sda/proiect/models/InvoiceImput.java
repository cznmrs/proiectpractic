package com.sda.proiect.models;

public class InvoiceImput {
    private  String productName;
    private Integer quantity;
    private ItemType itemType;

    public InvoiceImput(String productName, Integer quantity, ItemType itemType) {
        this.productName = productName;
        this.quantity = quantity;
        this.itemType = itemType;

    }

    public InvoiceImput() {

    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    @Override
    public String toString() {
        return "InvoiceImput{" +
                "productName='" + productName + '\'' +
                ", quantity=" + quantity +
                ", itemType=" + itemType +
                '}';
    }
}
