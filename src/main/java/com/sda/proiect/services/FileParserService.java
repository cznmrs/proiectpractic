package com.sda.proiect.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sda.proiect.models.InvoiceImput;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class FileParserService {
    public final ObjectMapper objectMapper;

    public FileParserService() {
        this.objectMapper = new ObjectMapper();
    }

    public List<InvoiceImput> readUserFile(String path){
        if (!checkJsonType(path)){
            throw new IllegalArgumentException("The file is not JSON type");
        }
        try {
            File file = new File(path);
            return objectMapper.readValue(file, objectMapper.getTypeFactory().constructCollectionType(List.class, InvoiceImput.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean checkJsonType(String path) {
        return path.toLowerCase().endsWith(".json");
    }
}
